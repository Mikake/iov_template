var GoogleMapsDemo = {
  init: function () {
    var t;
      t = new GMaps({
        div: "#m_gmap", lat: 22.626044, lng: 120.281760, click: function (t) {
          console.log(t)
        }
      });
      path = [[22.625750, 120.281607],
        [22.625633, 120.281848],
        [22.626599, 120.282345],
        [22.626389, 120.282777],
        [22.624439, 120.286917],
        [22.623991, 120.287682],
        [22.624144, 120.288563],
        [22.620729, 120.289239]],
        t.drawPolyline({
        path: path,
        strokeColor: "#131540",
        strokeOpacity: .6,
        strokeWeight: 6
      }),
        t.addMarker({
          lat: 22.626389,
          lng: 120.282777,
          title: "2019/03/05 16:05:30 車輛急煞",
          infoWindow: {content: '<span style="color:#000">2019/03/05 16:05:30 車輛急煞</span>'}
        }),
        t.addMarker({
          lat: 22.623991,
          lng: 120.287682,
          title: "2019/03/05 16:10:15 車輛急煞",
          infoWindow: {content: '<span style="color:#000">2019/03/05 16:10:15 車輛急煞</span>'}
        }), t.setZoom(16)
  }
};
jQuery(document).ready(function () {
  GoogleMapsDemo.init()
});
